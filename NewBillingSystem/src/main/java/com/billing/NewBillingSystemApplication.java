package com.billing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//public class NewBillingSystemApplication extends SpringBootServletInitializer {
//
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		return application.sources(NewBillingSystemApplication.class);
//	}
//
//	public static void main(String[] args) {
//		SpringApplication.run(NewBillingSystemApplication.class, args);
//	}
//
//}

public class NewBillingSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(NewBillingSystemApplication.class, args);
    }

}
