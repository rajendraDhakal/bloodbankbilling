package com.billing.api;

import com.billing.dto.BillListDto;
import com.billing.dto.ItemParameterDto;
import com.billing.model.Invoice;
import com.billing.repository.GroupRepository;
import com.billing.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class BillApi {
    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    GroupRepository groupRepository;


    @GetMapping("api/getBillBetweenDates")
    //  public Invoice  getBillBetweenDates(@RequestParam(name = "dateFrom")@DateTimeFormat(pattern = "yyyy-mm-dd")Date dateFrom, @RequestParam(name = "dateTo")@DateTimeFormat(pattern = "yyyy-mm-dd")Date dateTo){
    public List<BillListDto> getBillBetweenDates(@RequestParam("dateFrom") java.sql.Date dateFrom, @RequestParam("dateTo") java.sql.Date dateTo, @RequestParam("customerName") String customerName, @RequestParam("billNumber") String billNumber) throws ParseException {

        List<Invoice> invoice = new ArrayList<>();
        System.out.println("check api");
        System.out.println(dateFrom);
        System.out.println(dateTo);
        System.out.println("test1" + customerName.length());
        System.out.println("test32" + billNumber);

        if (billNumber != null & billNumber.length() > 0) {
            if (customerName != null & customerName.length() > 0) {
                customerName = customerName;
                billNumber = billNumber;

            } else {
                billNumber = billNumber;
                customerName = "%";
            }
            invoice = invoiceRepository.findBillBetweenDates(dateFrom, dateTo, customerName, Integer.parseInt(billNumber));
        }
        if (billNumber == null || billNumber.length() == 0) {
            if (customerName == null || customerName.length() == 0) {
                customerName = "%";
                invoice = invoiceRepository.findBillBetweenDates(dateFrom, dateTo, customerName);
            } else {
                invoice = invoiceRepository.findBillBetweenDates(dateFrom, dateTo, customerName);
            }
        }
        System.out.println("name" + customerName);
        System.out.println("invoice" + billNumber);
        List<BillListDto> billListDto = new ArrayList<>();
        //this is convert id to name like refertype come id we need  name so
        for (Invoice invoice1 : invoice) {
            List<ItemParameterDto> itemParameterDtos = new ArrayList<>();
            BillListDto billListDto1 = new BillListDto();
            billListDto1.setName(invoice1.getName());
            billListDto1.setInvoiceno(invoice1.getInvoiceno());
            billListDto1.setUnit(invoice1.getUnit());
            billListDto1.setTotal(invoice1.getTotal());
            billListDto1.setPaymenttype(invoice1.getPaymenttype());
            String groupName =invoiceRepository.findGroupNameByID(Integer.valueOf(invoice1.getRefertype()));
            billListDto1.setRefertype(groupName);
            int itemParameterSize = invoice1.getItemParameters().size();
            for (int j = 0; j < itemParameterSize; j++) {
//                System.out.println("j="+j);
                ItemParameterDto itemParameterDto = new ItemParameterDto();
                String ItemName = (invoice1.getItemParameters().get(j).getItemName());
                Float rate = invoice1.getItemParameters().get(j).getRate();
                itemParameterDto.setItemName(ItemName);
                itemParameterDto.setRate(rate);
                itemParameterDtos.add(itemParameterDto);
            }
            billListDto1.setItemParameterDtoList(itemParameterDtos);
            billListDto.add(billListDto1);
        }
        return billListDto;
    }
}