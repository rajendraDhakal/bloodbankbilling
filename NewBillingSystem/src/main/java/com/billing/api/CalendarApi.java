package com.billing.api;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;


import com.billing.dto.CalendarDTO;
import com.billing.dto.RestErrorDTO;
import com.billing.dto.RestWrapperDTO;
import com.billing.dto.YearListDto;
import com.billing.model.Calendar;
import com.billing.service.CalendarService;
import com.billing.utils.CalendarGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@RestController
@RequestMapping(value = "/api/utilities", produces = MediaType.APPLICATION_JSON)
public class CalendarApi {

	RestWrapperDTO wrapperDTO = new RestWrapperDTO();
	RestErrorDTO errorDTO = new RestErrorDTO();

	@Autowired
	private CalendarService cService;

	@GetMapping(value = "/calendars")
	public ResponseEntity<?> getAllYears() {
		YearListDto yearListDto = new YearListDto();
		List<String> yearList = cService.getAllYears();
		yearListDto.setYears(yearList);
//		System.out.println("calendar"+yearList);
		for (String calendar : yearList){
			System.out.println("check---->"+calendar);

		}
//
//		JsonObject o = new JsonObject();
//		o.add("years", new Gson().toJsonTree(yearList));
	//	System.out.println(o.toString());

		if (yearList.isEmpty()) {
			errorDTO.setStatus(false);
			errorDTO.setTitle("Null");
			errorDTO.setMessage("No Data Found");
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(errorDTO);
		}

		return ResponseEntity.ok(yearListDto);
	}

	@GetMapping(value = "/calendars/{year}")
	public ResponseEntity<?> getYearlyCalendar(@PathVariable("year") String year) {
		
			List<Calendar> cList = cService.getYearCalendar(year);

			JsonObject o = new JsonObject();
			o.add("calendars", new Gson().toJsonTree(cList));

			if (cList.isEmpty()) {
				errorDTO.setStatus(false);
				errorDTO.setTitle("Null");
				errorDTO.setMessage("No Data Found");
				return ResponseEntity.status(HttpStatus.NO_CONTENT).body(errorDTO);
			}
			return ResponseEntity.ok(cList);
	}

	@PostMapping(value = "/calendars")
	public ResponseEntity<?> postCalendar(CalendarDTO cDto, UriComponentsBuilder ucBuilder,
                                          HttpServletRequest request) throws Exception {

		List<Calendar> cList = CalendarGenerator.calendarGenerator(cDto);

		if (cService.generateAllCalendar(cList)) {

			wrapperDTO.setStatus(true);
			wrapperDTO.setTitle("Success");
			wrapperDTO.setMessage("Calendar Generated Successfully.");

			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(cDto.getBsStart().substring(0, 4)).toUri();

			return ResponseEntity.created(location).body(wrapperDTO);
		}

		errorDTO.setStatus(false);
		errorDTO.setTitle("Error");
		errorDTO.setMessage("Calendar Not Generated.");
		return ResponseEntity.status(HttpStatus.CONFLICT).body(errorDTO);

	}

	@DeleteMapping("/calendars/{year}")
	public ResponseEntity<?> deleteYearlyCalendar(@PathVariable("year") String year) {

		List<Calendar> cList = cService.getYearCalendar(year);

		if (cList.isEmpty()) {
			errorDTO.setStatus(false);
			errorDTO.setTitle("Error");
			errorDTO.setMessage("Calendar Not Deleted.");
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(errorDTO);
		}
		
		cList.stream().forEach(c -> cService.deleteCalendar(c));

		wrapperDTO.setStatus(true);
		wrapperDTO.setTitle("Success");
		wrapperDTO.setMessage("Calendar Deleted Successfully.");
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(wrapperDTO);
	}

}
