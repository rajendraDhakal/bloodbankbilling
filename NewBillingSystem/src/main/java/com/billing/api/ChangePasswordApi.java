package com.billing.api;


import com.billing.dto.ChangePasswordDTO;
import com.billing.dto.RestWrapperDTO;
import com.billing.model.User;
import com.billing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/change-password")
public class ChangePasswordApi {

	@Autowired
	private UserService uService;

	@GetMapping(value = "/verify")
	@ResponseBody
	public ResponseEntity<RestWrapperDTO> getOldPassword(@RequestParam("username") String username, @RequestParam("oldPassword") String oldPassword) {
		RestWrapperDTO wrapperDTO = new RestWrapperDTO();	
		try {
			User u = (User) uService.isValid(username, oldPassword);
			System.out.println(u);	
			if (u != null) {
				wrapperDTO.setStatus(true);
				wrapperDTO.setMessage("User found");
				wrapperDTO.setTitle("Valid");
				return new ResponseEntity<RestWrapperDTO>(wrapperDTO, HttpStatus.OK);
			}else { 
				wrapperDTO.setStatus(false);
				wrapperDTO.setMessage("User not found");
				wrapperDTO.setTitle("Invalid");
				return new ResponseEntity<RestWrapperDTO>(wrapperDTO, HttpStatus.OK);
			}
		} catch (Exception e) {
			wrapperDTO.setStatus(false);
			wrapperDTO.setMessage("Something went wrong" + e.getMessage());
			wrapperDTO.setTitle("Failed");
			return new ResponseEntity<RestWrapperDTO>(wrapperDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping("/change")
	public ResponseEntity<?> putPassword(@RequestBody ChangePasswordDTO pDto) {
		try {
			if (uService.changePassword(pDto)) {
				return ResponseEntity.status(HttpStatus.ACCEPTED).build();
			}
			return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

}
