package com.billing.api;

import com.billing.dto.BillListDto;
import com.billing.dto.ItemParameterDto;
import com.billing.model.Invoice;
import com.billing.model.ItemParameter;
import com.billing.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CreditStatmentApi {
    @Autowired
    InvoiceRepository invoiceRepository;


    @GetMapping("api/getCreditStReportBetweenDates")
    public List<BillListDto> getBillBetweenDates(@RequestParam("dateFrom") Date dateFrom, @RequestParam("dateTo") Date dateTo) throws ParseException {

        List<BillListDto> billListDtos = new ArrayList<>();
        List<Invoice> invoices = invoiceRepository.findCreditStBetweenDates(dateFrom, dateTo);
        for (Invoice invoice : invoices) {
            BillListDto billListDto = new BillListDto();
            List<ItemParameterDto> itemParameterDtos = new ArrayList<>();
            for (ItemParameter items : invoice.getItemParameters()) {
                ItemParameterDto itemParameterDto = new ItemParameterDto();
                itemParameterDto.setItemName(items.getItemName());
                itemParameterDto.setRate(items.getRate());
                itemParameterDtos.add(itemParameterDto);
            }
            billListDto.setItemParameterDtoList(itemParameterDtos);
            billListDto.setInvoiceno(invoice.getInvoiceno());
            billListDto.setName(invoice.getName());
            String groupName =invoiceRepository.findGroupNameByID(Integer.valueOf(invoice.getRefertype()));
            billListDto.setRefertype(groupName);
            billListDto.setUnit(invoice.getUnit());
            billListDto.setTotal(invoice.getTotal());
            billListDto.setPaymenttype(invoice.getPaymenttype());
            if(invoice.getPaymenttype().equalsIgnoreCase("credit")){
                billListDtos.add(billListDto);
            }

        }

        return billListDtos;
    }

}
