package com.billing.api;


import com.billing.model.Invoice;
import com.billing.service.RetriveCurrentDateService;
import com.billing.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class DailyCashCollectionApi {
    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    RetriveCurrentDateService retriveCurrentDateService;

    @GetMapping("api/getDailyCashCollection")
    public List<Invoice> findByInvoiceDate() throws ParseException {
        List<Invoice> byInvoiceDate = invoiceRepository.findByInvoiceDate(retriveCurrentDateService.currentDate());
        System.out.println("test12"+byInvoiceDate.toString());
        List<Invoice> invoices = new ArrayList<>();
        for (Invoice invoiceno :byInvoiceDate){
            System.out.println(invoiceno.getPaymenttype());
            if(invoiceno.getPaymenttype().equalsIgnoreCase("cash")){
               invoices.add(invoiceno);
            }
        }

        return invoices;
    }

}
