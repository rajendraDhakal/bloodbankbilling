package com.billing.api;

import com.billing.dto.CashCollectionDto;
import com.billing.dto.ItemParameterDto;
import com.billing.model.Invoice;
import com.billing.model.ItemParameter;
import com.billing.repository.GroupRepository;
import com.billing.repository.InvoiceRepository;
import com.billing.repository.RefundRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class InvoiceCashCollectionReportApi {
    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    RefundRepository refundRepository;

    @GetMapping("api/getInvoiceReportBetweenDates")
    public List<CashCollectionDto> getBillBetweenDates(@RequestParam("dateFrom") Date dateFrom, @RequestParam("dateTo") Date dateTo) throws ParseException {
        List<Invoice> invoice = invoiceRepository.findBillBetweenDates(dateFrom, dateTo);
        List<CashCollectionDto> cashCollectionDtos = new ArrayList<>();
        for (Invoice invoice1 : invoice) {
            CashCollectionDto cashCollectionDto = new CashCollectionDto();
            List<ItemParameterDto> itemParameterDtos = new ArrayList<>();
            for (ItemParameter item : invoice1.getItemParameters()) {
                ItemParameterDto itemParameterDto = new ItemParameterDto();
                itemParameterDto.setItemName(item.getItemName());
                itemParameterDto.setRate(item.getRate());
                itemParameterDtos.add(itemParameterDto);

            }
            cashCollectionDto.setItemParameterDtoList(itemParameterDtos);
            cashCollectionDto.setInvoiceno(invoice1.getInvoiceno());
            cashCollectionDto.setName(invoice1.getName());
            cashCollectionDto.setPaymenttype(invoice1.getPaymenttype());
            String groupName =invoiceRepository.findGroupNameByID(Integer.valueOf(invoice1.getRefertype()));
            cashCollectionDto.setRefertype(groupName);
            cashCollectionDto.setUnit(invoice1.getUnit());
            cashCollectionDto.setTotal(invoice1.getTotal());
            cashCollectionDto.setType("invoice");
            if (invoice1.getPaymenttype().equalsIgnoreCase("cash")){
                cashCollectionDtos.add(cashCollectionDto);
            }

        }

        return cashCollectionDtos;
    }


}
