package com.billing.api;


import com.billing.dto.RefundDto;
import com.billing.dto.RefundItemDto;
import com.billing.model.Refund;
import com.billing.repository.GroupRepository;
import com.billing.repository.RefundRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController

public class RefundApi {
    @Autowired
    RefundRepository refundRepository;
    @Autowired
    GroupRepository groupRepository;

    @GetMapping("api/getRefundBetweenDates")
    //  public Invoice  getBillBetweenDates(@RequestParam(name = "dateFrom")@DateTimeFormat(pattern = "yyyy-mm-dd")Date dateFrom, @RequestParam(name = "dateTo")@DateTimeFormat(pattern = "yyyy-mm-dd")Date dateTo){
    public List<RefundDto> findRefundBetweenDates(@RequestParam("dateFrom") java.sql.Date dateFrom, @RequestParam("dateTo") java.sql.Date dateTo, @RequestParam("customerName") String customerName, @RequestParam("refundNo") String refundNo) throws ParseException {
        List<Refund> refund = new ArrayList<>();
        System.out.println("check api");
        System.out.println(dateFrom);
        System.out.println(dateTo);
        System.out.println("name" + customerName);
        System.out.println("number" + refundNo);
        if (refundNo != null & refundNo.length() > 0) {
            if (customerName != null & customerName.length() > 0) {
                refundNo = refundNo;
                customerName = customerName;
            } else {
                refundNo = refundNo;
                customerName = "%";
            }
            refund = refundRepository.findRefundBetweenDates(dateFrom, dateTo, customerName, Integer.parseInt(refundNo));
        }
        if (refundNo == null || refundNo.length() == 0) {
            if (customerName == null || customerName.length() == 0) {
                customerName = "%";
                refund = refundRepository.findRefundBetweenDates(dateFrom, dateTo, customerName);
            } else {
                refund = refundRepository.findRefundBetweenDates(dateFrom, dateTo, customerName);
            }
        }

        List<RefundDto> refundDto = new ArrayList<>();
        for (Refund refund1 : refund) {
            List<RefundItemDto> refundItemDtos = new ArrayList<>();
            RefundDto refundDto1 = new RefundDto();
            refundDto1.setName(refund1.getName());
            refundDto1.setInvoiceno(refund1.getInvoiceno());
            refundDto1.setUnit(refund1.getUnit());
            refundDto1.setTotal(refund1.getTotal());
            refundDto1.setPaymenttype(refund1.getPaymenttype());
//            GroupParameter gp =groupRepository.findGroupParameterById(Integer.valueOf(refund1.getRefertype()));
//            String groupName = gp.getGroupName();
            refundDto1.setRefertype(refund1.getRefertype());
            int itemParameterSize = refund1.getItemParameters().size();
            for (int j = 0; j < itemParameterSize; j++) {
//                System.out.println("j="+j);
                RefundItemDto refundItemDto = new RefundItemDto();
                String ItemName = (refund1.getItemParameters().get(j).getItemName());
                Float rate = refund1.getItemParameters().get(j).getRate();
                refundItemDto.setItemName(ItemName);
                refundItemDto.setRate(rate);
                refundItemDtos.add(refundItemDto);
            }
            refundDto1.setRefundItemDtoList(refundItemDtos);
            refundDto.add(refundDto1);
        }

        return refundDto;
    }

}
