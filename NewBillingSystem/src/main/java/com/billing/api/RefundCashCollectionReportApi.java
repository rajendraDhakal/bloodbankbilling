package com.billing.api;

import com.billing.dto.CashCollectionDto;
import com.billing.dto.ItemParameterDto;
import com.billing.model.ItemParameter;
import com.billing.model.Refund;
import com.billing.repository.GroupRepository;
import com.billing.repository.RefundRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RefundCashCollectionReportApi {
    @Autowired
    RefundRepository refundRepository;
    @Autowired
    GroupRepository groupRepository;

    @GetMapping("api/getRefundReportBetweenDates")
    public List<CashCollectionDto> getBillBetweenDates(@RequestParam("dateFrom") Date dateFrom, @RequestParam("dateTo") Date dateTo) throws ParseException {

        List<CashCollectionDto> cashCollectionDtos = new ArrayList<>();
        List<Refund> refunds = refundRepository.findRefundBetweenDates(dateFrom, dateTo);
        for (Refund refund : refunds) {
            CashCollectionDto cashCollectionDto = new CashCollectionDto();
            List<ItemParameterDto> itemParameterDtos = new ArrayList<>();
            for (ItemParameter items : refund.getItemParameters()) {
                ItemParameterDto itemParameterDto = new ItemParameterDto();
                itemParameterDto.setItemName(items.getItemName());
                itemParameterDto.setRate(items.getRate());
                itemParameterDtos.add(itemParameterDto);
            }
            cashCollectionDto.setItemParameterDtoList(itemParameterDtos);
            cashCollectionDto.setInvoiceno(refund.getInvoiceno());
            cashCollectionDto.setName(refund.getName());
            cashCollectionDto.setPaymenttype(refund.getPaymenttype());
            cashCollectionDto.setRefertype(refund.getRefertype());
            cashCollectionDto.setUnit(refund.getUnit());
            cashCollectionDto.setTotal(refund.getTotal());
            cashCollectionDto.setType("refund");
            cashCollectionDtos.add(cashCollectionDto);
        }

        return cashCollectionDtos;
    }
}
