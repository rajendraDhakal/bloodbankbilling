package com.billing.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.billing.dto.ItemAndGroupDto;
import com.billing.model.GroupParameter;
import com.billing.model.ItemParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.billing.repository.GroupRepository;
import com.billing.repository.ItemParameterRepository;

import javax.servlet.http.HttpServletRequest;

@RestController
public class RestApis {
	@Autowired
	ItemParameterRepository itemParameterRepository;
	@Autowired
	GroupRepository groupRepository;


	@GetMapping("api/getAllItemParameter")
	public List<ItemParameter> getAllItemParameter() {
		System.out.println("items--->"+itemParameterRepository.findAll());
		return itemParameterRepository.findAll();
	}
	
	@GetMapping("api/getAllGroupParameter")
	public List<GroupParameter> getAllGroupParameter() {
		return groupRepository.findAll();
	}

	@GetMapping("api/getGroupNameById")
	public Optional<GroupParameter> getGroupNameById(@RequestParam(name = "id") int id) {
		return groupRepository.findById(id);
	}
	@GetMapping("api/getItemRateById")
	public int getItemRateById(@RequestParam(name = "id") int id){
		int rate=itemParameterRepository.findRateByID(id);
		return rate;
	}

	@GetMapping("api/getGroupIdByName")
	public GroupParameter getGroupIdByName(@RequestParam(name = "groupName") String groupName) {
		return groupRepository.findByGroupName(groupName);
	}

	@GetMapping("api/getAllItemsAndGroups")
	public List<ItemAndGroupDto> getAllItemsAndGroups() {
		List<ItemAndGroupDto> itemAndGroupDto = new ArrayList<>();
		for(ItemParameter items: itemParameterRepository.findAll()) {
			ItemAndGroupDto igdto = new ItemAndGroupDto();
			igdto.setId(items.getId());
			igdto.setName(items.getItemName());
			igdto.setType("item");
			itemAndGroupDto.add(igdto);
		}
		for(GroupParameter groups: groupRepository.findAll()) {
			ItemAndGroupDto igdto = new ItemAndGroupDto();
			igdto.setId(groups.getId());
			igdto.setName(groups.getGroupName());
			igdto.setType("group");
			itemAndGroupDto.add(igdto);
		}
		return itemAndGroupDto;
		

	}
	@GetMapping("api/getItemParametersByGroupId")
	public List<ItemParameter> getGroupIdByName(HttpServletRequest request) {
		int groupParameterId = Integer.parseInt(request.getParameter("groupParameterId"));
		List<Integer> listItemParameterId = itemParameterRepository.getItemParameterIdByGroupParametersId(groupParameterId);
		List<ItemParameter> itemParameters = itemParameterRepository.getItemParameterByItemParameterIds(listItemParameterId);

		return itemParameters;



	}


//	@GetMapping("/api/getItemsByGroupId")
//	public List<ItemParameter> getItemsByGroupId(@RequestParam(name = "groupId") int groupId){
//		//return itemParameterRepository.findByGroupParameterId(groupId);
//
//	}

}
