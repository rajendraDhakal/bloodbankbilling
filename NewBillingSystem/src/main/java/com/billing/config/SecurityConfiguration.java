package com.billing.config;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private AuthenticationSuccessHandler authenticationSuccessHandler;

	private static final String[] PUBLIC_MATCHERS = {"/css/**", "/fonts/**", "/icons/**", "/images/**", "/js/**", "/api/**", "/register/","/change-password"};
	
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	public void WebSecurityConfig(AuthenticationSuccessHandler authenticationSuccessHandler) {
		this.authenticationSuccessHandler = authenticationSuccessHandler;
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers(PUBLIC_MATCHERS).permitAll()
			.antMatchers("/User/**").access("hasAuthority('USER')")
			.antMatchers("/Admin/**").access("hasAuthority('ADMIN')")
			.antMatchers("/register").access("hasAuthority('ADMIN')")

//			.antMatchers("/admin").hasAuthority("MANAGER")
//			.antMatchers("/admin/parameter/**").hasAuthority("MANAGER")
//			.antMatchers("/admin/**").hasAuthority("EXECUTIVE")
//			.anyRequest().authenticated()
			.and().formLogin().loginPage("/login").permitAll().successHandler(authenticationSuccessHandler).and()
			.logout().logoutUrl("/appLogout").logoutSuccessUrl("/login").and().csrf().disable();
	}
	
	@Override
	@Bean()
	public AuthenticationManager authenticationManagerBean() throws Exception {
		System.out.println("check");
		return super.authenticationManagerBean();
	}


}
