package com.billing.controller;

import javax.servlet.http.HttpSession;


import com.billing.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ChangePasswordController {

	@GetMapping("/reset-password")
	public String getChangePassword(Model model, HttpSession session) {
		
		User user = (User) session.getAttribute("suser");
		
		model.addAttribute("user", user);
		
		return "change-password";
	}
	
}
