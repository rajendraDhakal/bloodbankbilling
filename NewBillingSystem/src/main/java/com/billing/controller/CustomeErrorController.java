package com.billing.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomeErrorController implements ErrorController {

	@Override
	public String getErrorPath() {
		return "/error";
	}

	@GetMapping("/error")
	public String handleError(HttpServletRequest request) {
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		
		if (status != null) {
			Integer statusCode = Integer.valueOf(status.toString());

			if (statusCode == HttpStatus.BAD_REQUEST.value()) {
				return "error/page-error-400";
			} else if (statusCode == HttpStatus.UNAUTHORIZED.value()) {
				return "error/page-error-401";
			} else if (statusCode == HttpStatus.FORBIDDEN.value()) {
				return "error/page-error-403";
			} else if (statusCode == HttpStatus.NOT_FOUND.value()) {
				return "error/page-error-404";
			} else if (statusCode == HttpStatus.METHOD_NOT_ALLOWED.value()) {
				return "error/page-error-405";
			} else if (statusCode == HttpStatus.REQUEST_TIMEOUT.value()) {
				return "error/page-error-408";
			} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
				return "error/page-error-500";
			} else if (statusCode == HttpStatus.BAD_GATEWAY.value()) {
				return "error/page-error-502";
			} else if (statusCode == HttpStatus.SERVICE_UNAVAILABLE.value()) {
				return "error/page-error-503";
			} else if (statusCode == HttpStatus.GATEWAY_TIMEOUT.value()) {
				return "error/page-error-504";
			} else if (statusCode == HttpStatus.HTTP_VERSION_NOT_SUPPORTED.value()) {
				return "error/page-error-505";
			}
		}
		return "error";
	}

}
