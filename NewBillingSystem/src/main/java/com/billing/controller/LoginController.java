package com.billing.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

	@GetMapping(value = {"/", "/login"})
	public String getLogin(HttpSession session) {
		session.invalidate();
		return "login_page";
	}
	
	/*
	 * @PostMapping("/login") public String formPost(@ModelAttribute Login signin) {
	 * System.out.println(signin.toString());
	 * 
	 * return ("login_page");
	 * 
	 * }
	 */

	@GetMapping(value = {"/logout"})
	public String getLogout(HttpSession session) {
		session.invalidate();
		return "redirect:/login";
	}
}
