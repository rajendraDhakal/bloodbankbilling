package com.billing.controller;

import com.billing.model.FiscalYear;
import com.billing.model.Invoice;
import com.billing.repository.FiscalYearRepository;
import com.billing.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class OperationController {

    @Autowired
    InvoiceService invoiceService;
    @Autowired
    FiscalYearRepository fiscalYearRepository;

    @GetMapping("/operation")
    public String operationInvoice(Model model) {
        model.addAttribute("invoice", new Invoice());
        return ("Operation/invoice-form");
    }

    @PostMapping("/operation")
    public String operationInvoicePost(@ModelAttribute Invoice invoice, RedirectAttributes redirectAttributes) {
        System.out.println("test" + invoice.getItemParameters().size());
        // to remove one blank row generated in fronend before saving into database
        Invoice finalInvoice = invoiceService.RemoveRowInvoice(invoice);
        FiscalYear fiscalYear = fiscalYearRepository.findByStatus("y");
        finalInvoice.setFiscalYear(fiscalYear);
        invoiceService.saveInvoice(finalInvoice);
        redirectAttributes.addFlashAttribute("invoice", finalInvoice);
        return ("redirect:/operation/print_invoice");
    }


    @GetMapping("/operation/print_invoice")
    public String operationPrint(Model model, @ModelAttribute("invoice") Invoice invoice) {
        model.addAttribute("list", invoice);
        return ("Operation/invoic_print");

    }
}
