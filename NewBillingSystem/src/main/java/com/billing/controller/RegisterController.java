package com.billing.controller;


import com.billing.dto.UserDTO;
import com.billing.model.User;
import com.billing.repository.UserRepository;
import com.billing.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegisterController {
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
    UserRepository userRepository;
	
	@Autowired
    RoleService rService;

	@GetMapping(value = "/register")
	public String registerbyAdmin(Model model) {
		// This list (ulist) should be actually retrieved from database
//		List<String> ulist = new ArrayList<>();
//		ulist.add("Admin");
//		ulist.add("User");
		
		model.addAttribute("user", new UserDTO());
		model.addAttribute("rolelist", rService.getAllRoles());
		return "register";
	}

	@PostMapping("/register")
	public String registeruser(@ModelAttribute UserDTO user) {
//		Set<Role> roles = new HashSet<>();
		User userinfo = new User();
		userinfo.setFullName(user.getFullName());
		userinfo.setUsername(user.getUsername());
		userinfo.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		
		// this need not be done using if else when role list is retrieved from database
		// in registerbyadmin method above
//		for (Role role : user.getRolelist()) {
//			Role r = new Role();
//			if (role.equals("Admin")) {
//				r.setName(role);
//				r.setId(1);
//				roles.add(r);
//			} else {
//				r.setName(role);
//				r.setId(2);
//				roles.add(r);
//
//			}
//
//		}

		userinfo.setRoles(user.getRolelist());
		userRepository.save(userinfo);

		return "redirect:/login";

	}

}
