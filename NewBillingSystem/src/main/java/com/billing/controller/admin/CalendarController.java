package com.billing.controller.admin;

import java.io.IOException;

import com.billing.model.FiscalYear;
import com.billing.repository.FiscalYearRepository;
import com.billing.service.CalendarService;
import com.billing.service.FiscalYearService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Controller
@RequestMapping(value = "/Admin/utilities")
public class CalendarController {

	@Autowired
	private CalendarService cService;
	@Autowired
	private FiscalYearService fiscalYearService;
	@Autowired
	private FiscalYearRepository fiscalYearRepository;

	@GetMapping("/calendar")
	public String getCalendar(Model model) throws Exception, IOException {
		model.addAttribute("yearList", cService.getAllYears());
		return "Admin/utilities/calendar";
	}	

	@GetMapping("/calendar/query")
	@ResponseBody
	public JsonObject getCalendarDetail(@RequestParam("year") String year, Model model) throws Exception, IOException {		
		JsonObject o = new JsonObject();
		o.add("calendars", new Gson().toJsonTree(cService.getYearCalendar(year)));
		return o;
	}
	
	
	@GetMapping(value = "/fiscal-year")
	public String getFiscalYear(Model m) {
		System.out.println("fiscal year controller");
		m.addAttribute("fiscalYearList", fiscalYearService.getFiscalYear());
		m.addAttribute("fiscalYearModel",new FiscalYear());
		return "Admin/utilities/fiscal-year";
	}

	@PostMapping(value = "/fiscal-year")
	public String checkForm(@ModelAttribute FiscalYear fiscalyear, RedirectAttributes atts) {
		try {

			fiscalyear.setFyStartAd(fiscalYearService.getAD(fiscalyear.getFyStartBs()));
			fiscalyear.setFyEndAd(fiscalYearService.getAD(fiscalyear.getFyEndBs()));

			fiscalYearService.addFY(fiscalyear);
			atts.addFlashAttribute("success", "fiscal year created");

			return "redirect:/Admin/utilities/fiscal-year";

		} catch (Exception e) {
			System.out.println(e.getMessage());
			atts.addFlashAttribute("error", "fiscal year not created");

			return "redirect:/Admin/utilities/fiscal-year";
		}

	}
	
	@GetMapping(value="/delete/{id}")
	public String deleteFY(@PathVariable(value = "id") String fyId,RedirectAttributes atts) {
		FiscalYear fy=fiscalYearService.getFiscalYearByFy(fyId);
		if(fy!=null) {
			fiscalYearService.deleteFY(fy);
		}else {
			atts.addFlashAttribute("error", "No data found");
		}
		return "redirect:/Admin/utilities/fiscal-year";
	}
	@GetMapping("/edit/{id}")
	public String updateFY(@PathVariable("id") String id, Model model) {
		System.out.println("edit/id controllers");
		FiscalYear fy = fiscalYearRepository.findById((String)id).orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
		model.addAttribute("fiscalYear", fy);
		return "fiscal";
	}
	@PostMapping("update/{id}")
	public String updateStudent(@PathVariable("id") String id,@ModelAttribute FiscalYear fy, Model model) {
		fy.setFyStartAd(fiscalYearService.getAD(fy.getFyStartBs()));
		fy.setFyEndAd(fiscalYearService.getAD(fy.getFyEndBs()));
		fiscalYearRepository.save(fy);
		model.addAttribute("fiscalYearList",fiscalYearRepository.findAll());
		model.addAttribute("fiscalYearModel", new FiscalYear());
		return "/Admin/utilities/fiscal-year";
	}


//@PostMapping(value="/addFY")
//public String saveFY(@ModelAttribute("fiscalYear") FiscalYear fy) {
//  fiscalYearService.addFY(fy);
//    return "redirect:/admin/utilities/fiscal-year";
//}
}

//
//@PostMapping("/fiscal-year")
//public String postFY(@ModelAttribute FiscalYear fy, @Valid FiscalYear fiscalyear, BindingResult result) {
//if (result.hasErrors()) {
//	return "admin/utilities/fiscal-year";
//}
//
//if (fiscalYearService.addFY(fy)) {
//	
//	return "admin/utilities/fiscal-year";
//} else {
//	return "admin/utilities/fiscal-year";
//}
//}
//
//@RequestMapping(value = "/addFY", method = RequestMethod.POST)
//public ModelAndView saveFY(@ModelAttribute FiscalYear fy, BindingResult result) {
//	ModelAndView mv = new ModelAndView("redirect:/admin/utilities/fiscal-year");
//
//	if (result.hasErrors()) {
//		return new ModelAndView("error");
//	}
//	boolean isAdded = fiscalYearService.addFY(fy);
//	if (isAdded) {
//		mv.addObject("message", "New user successfully added");
//	} else {
//		return new ModelAndView("error");
//	}
//
//	return mv;
//}
//
//@RequestMapping(value = "/editFy/{fy}", method = RequestMethod.GET)
//public ModelAndView displayEditUserForm(@PathVariable String fy) {
//	ModelAndView mv = new ModelAndView("/editUser");
//	FiscalYear fiscalYear = fiscalYearService.getFiscalYearByFy(fy);
//	mv.addObject("headerMessage", "Edit FY Details");
//	mv.addObject("fiscalYear", fiscalYear);
//	return mv;
//}
//
//@RequestMapping(value = "/deleteFy/{fy}", method = RequestMethod.GET)
//public ModelAndView deleteUserById(@PathVariable String fy) {
//	boolean isDeleted = fiscalYearService.deleteFY(fy);
//	System.out.println("Fy deletion response: " + isDeleted);
//	ModelAndView mv = new ModelAndView("redirect:/admin/utilities/fiscal-year");
//	return mv;
//
//}




