package com.billing.controller.admin;

import com.billing.model.GroupParameter;
import com.billing.model.ItemListWrapper;
import com.billing.model.ItemParameter;
import com.billing.model.ItemToGroupParameter;
import com.billing.repository.GroupRepository;
import com.billing.repository.ItemParameterRepository;
import com.billing.service.GroupService;
import com.billing.service.ItemParameterService;
import com.billing.service.ItemToGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class GroupAndItemController {

    @Autowired
    GroupRepository groupRepository;
    @Autowired
    ItemParameterRepository itemParameterRepository;
    @Autowired
    GroupService groupService;
    @Autowired
    ItemToGroupService itemToGroupService;
    @Autowired
    ItemParameterService itemParameterService;

    @GetMapping("/Admin/group")
    public String group(Model model) {
        model.addAttribute("groupParameter", new GroupParameter());
        return ("Admin/Parameter/item_group");
    }

    @PostMapping("/Admin/group")
    public String itemGroup(@ModelAttribute GroupParameter groupParameter, @ModelAttribute("itemToGroupParameter") ItemToGroupParameter itemTOGroupParameter) {
        System.out.println(groupParameter.toString());
        groupService.groupSave(groupParameter);
        return ("Admin/Parameter/item_group");

    }

    @PostMapping("/Admin/itemtogroup")
    public String addItemGroup(@ModelAttribute("groupParameter") GroupParameter groupParameter) {
//        groupRepository.save(groupParameter);
        itemToGroupService.saveItemToGroup(groupParameter);
        return ("Admin/Parameter/item_group");
    }

    @GetMapping("/Admin/parameter")
    public String item(Model model) {

        model.addAttribute("itemListWrapper", new ItemListWrapper());
        return ("Admin/Parameter/item_parameter");
    }

    @PostMapping("/Admin/parameter")
    public String itemSubmit(@ModelAttribute ItemListWrapper itemListWrapper) {
        for (ItemParameter itemParameter : itemListWrapper.getItems()) {
           // itemParameterRepository.save(itemParameter);
            itemParameterService.saveItemParameter(itemParameter);
        }

        return ("Admin/Parameter/item_parameter");

    }


}
