package com.billing.controller.admin;


import com.billing.dto.InvoiceNumberDto;
import com.billing.model.FiscalYear;
import com.billing.model.Invoice;
import com.billing.model.Refund;
import com.billing.repository.FiscalYearRepository;
import com.billing.repository.InvoiceRepository;
import com.billing.service.InvoiceService;
import com.billing.service.RefundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;


@Controller
public class RefundController {

    @Autowired
    RefundService refundService;
    @Autowired
    InvoiceService invoiceService;
    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    FiscalYearRepository fiscalYearRepository;


    @GetMapping("/operation/refund")
    public String refund(Model model) {
        model.addAttribute("invoiceno", new InvoiceNumberDto());
        return ("Operation/refund_invoiceno");
    }

    @PostMapping("/operation/refund")
    public ModelAndView refunds(@ModelAttribute InvoiceNumberDto invoiceNumberDto) {
        // search invoiceNumber
        Optional<Invoice> invoiceData = invoiceService.searchByInvoiceId(invoiceNumberDto.getInvoiceNo());
        System.out.println("invoiceData"+invoiceData.get().getRefertype());
        String groupName = invoiceRepository.findGroupNameByID(Integer.valueOf(invoiceData.get().getRefertype()));
        invoiceData.get().setRefertype(groupName);
        Float unit = Float.valueOf(invoiceData.get().getUnit());
        ModelAndView modelAndView = new ModelAndView();
        System.out.println("invoiceData------>"+invoiceData);
//        modelAndView.addObject("refundData", invoiceData);
        invoiceData.ifPresent(o ->{
            modelAndView.addObject("refundData",o);
        });
        modelAndView.setViewName("Operation/refund_form");
        return modelAndView;
    }

    @PostMapping("/operation/refund_form")
    public String refunds(@ModelAttribute Refund refund, RedirectAttributes redirectAttributes) {
        Refund saveNewRefund = refundService.removeUncheckedItemParameters(refund);
        FiscalYear fiscalYear = fiscalYearRepository.findByStatus("y");
        saveNewRefund.setFiscalYear(fiscalYear);
        refundService.saveRefund(saveNewRefund);
        System.out.println("saveNewRefund"+saveNewRefund.toString());
        redirectAttributes.addFlashAttribute("refundData", saveNewRefund);
        return ("redirect:/operation/refund_print");
    }

    @GetMapping("/operation/refund_print")
    public String print(Model model, @ModelAttribute("refundData") Refund refund) {
        System.out.println("list" + refund.toString());
        model.addAttribute("list", refund);
        return ("Operation/refund_print");
    }

}
