package com.billing.controller.admin;



import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ReportController {


    @RequestMapping("/reports/bill")
    public ModelAndView report () {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Admin/Reports/bill");
        return modelAndView;
    }
    @RequestMapping("/reports/refund")
    public ModelAndView report1 () {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Admin/Reports/refund");
        return modelAndView;
    }
    @RequestMapping("/reports/receipt")
    public ModelAndView report2 () {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Admin/Reports/receipt");
        return modelAndView;
    }
    @RequestMapping("/reports/cash-collection")
    public ModelAndView report3 () {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Admin/Reports/cashCollection");
        return modelAndView;
    }
    @RequestMapping("/reports/refund-statment")
    public ModelAndView report4 () {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Admin/Reports/refundStatment");
        return modelAndView;
    }
    @RequestMapping("/reports/credit-bill-statment")
    public ModelAndView report5 () {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Admin/Reports/creditStatment");
        return modelAndView;
    }
    @RequestMapping("/reports/daily-cash-collection")
    public ModelAndView report6 () {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Admin/Reports/dailyCashCollection");
        return modelAndView;
    }
}
