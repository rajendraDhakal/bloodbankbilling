package com.billing.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

public class CalendarDTO {
	
	private String bsStart;
	private String adStart;
	private int baisakh;
	private int jestha;
	private int ashar;
	private int shrawan;
	private int bhadra;
	private int asoj;
	private int kartik;
	private int mangsir;
	private int poush;
	private int magh;
	private int falgun;
	private int chaitra;

	public String getBsStart() {
		return bsStart;
	}

	public void setBsStart(String bsStart) {
		this.bsStart = bsStart;
	}

	public String getAdStart() {
		return adStart;
	}

	public void setAdStart(String adStart) {
		this.adStart = adStart;
	}

	public int getBaisakh() {
		return baisakh;
	}

	public void setBaisakh(int baisakh) {
		this.baisakh = baisakh;
	}

	public int getJestha() {
		return jestha;
	}

	public void setJestha(int jestha) {
		this.jestha = jestha;
	}

	public int getAshar() {
		return ashar;
	}

	public void setAshar(int ashar) {
		this.ashar = ashar;
	}

	public int getShrawan() {
		return shrawan;
	}

	public void setShrawan(int shrawan) {
		this.shrawan = shrawan;
	}

	public int getBhadra() {
		return bhadra;
	}

	public void setBhadra(int bhadra) {
		this.bhadra = bhadra;
	}

	public int getAsoj() {
		return asoj;
	}

	public void setAsoj(int asoj) {
		this.asoj = asoj;
	}

	public int getKartik() {
		return kartik;
	}

	public void setKartik(int kartik) {
		this.kartik = kartik;
	}

	public int getMangsir() {
		return mangsir;
	}

	public void setMangsir(int mangsir) {
		this.mangsir = mangsir;
	}

	public int getPoush() {
		return poush;
	}

	public void setPoush(int poush) {
		this.poush = poush;
	}

	public int getMagh() {
		return magh;
	}

	public void setMagh(int magh) {
		this.magh = magh;
	}

	public int getFalgun() {
		return falgun;
	}

	public void setFalgun(int falgun) {
		this.falgun = falgun;
	}

	public int getChaitra() {
		return chaitra;
	}

	public void setChaitra(int chaitra) {
		this.chaitra = chaitra;
	}

}
