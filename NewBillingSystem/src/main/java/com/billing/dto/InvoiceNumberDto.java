package com.billing.dto;

public class InvoiceNumberDto {
    private Integer invoiceNo;

    public Integer getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(Integer invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
}
