package com.billing.dto;

import java.util.List;

public class ItemParameterDto {
    private String itemName;
    private Float rate;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }
}
