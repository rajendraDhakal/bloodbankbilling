package com.billing.dto;

import java.util.List;

public class RefundDto {
    private Integer invoiceno;
    private String name;
    private String paymenttype;
    private String refertype;
    private String unit;
    private Double total;
    private List<RefundItemDto> refundItemDtoList;


    public Integer getInvoiceno() {
        return invoiceno;
    }

    public void setInvoiceno(Integer invoiceno) {
        this.invoiceno = invoiceno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }

    public String getRefertype() {
        return refertype;
    }

    public void setRefertype(String refertype) {
        this.refertype = refertype;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<RefundItemDto> getRefundItemDtoList() {
        return refundItemDtoList;
    }

    public void setRefundItemDtoList(List<RefundItemDto> refundItemDtoList) {
        this.refundItemDtoList = refundItemDtoList;
    }
}
