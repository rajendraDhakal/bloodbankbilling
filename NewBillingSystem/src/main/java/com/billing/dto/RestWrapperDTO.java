package com.billing.dto;

public class RestWrapperDTO {

	protected boolean status;
	private String title;
	private String message;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean value) {
		status = value;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "RestWrapperDTO [status=" + status + ", title=" + title + ", message=" + message + "]";
	}

}
