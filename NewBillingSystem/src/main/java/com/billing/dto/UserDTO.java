package com.billing.dto;

import java.util.Set;

import com.billing.model.Role;


public class UserDTO {
    private String fullName;
    private String username;
    private String password;
    private Set<Role> rolelist;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRolelist() {
        return rolelist;
    }

    public void setRolelist(Set<Role> rolelist) {
        this.rolelist = rolelist;
    }
}
