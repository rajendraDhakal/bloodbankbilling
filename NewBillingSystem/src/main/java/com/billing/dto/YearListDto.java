package com.billing.dto;

import java.util.List;

public class YearListDto {
    public List<String> years;

    public List<String> getYears() {
        return years;
    }

    public void setYears(List<String> years) {
        this.years = years;
    }
}
