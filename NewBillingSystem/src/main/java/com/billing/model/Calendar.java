package com.billing.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
public class Calendar implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8439668770656810645L;
	@Id
	@Temporal(TemporalType.DATE)
	private Date adDate;
	@NotNull
	@Column(name = "bs_date", unique = true, length = 10, updatable = false)
	private String bsDate;
	@NotNull
	@Column(length = 10, updatable = false)
	private String day;
	@Column(updatable = false)
	private boolean holiday;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public void setAdDate(Date adDate) {
		this.adDate = adDate;
	}

	public String getBsDate() {
		return bsDate;
	}

	public void setBsDate(String bsDate) {
		this.bsDate = bsDate;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public boolean isHoliday() {
		return holiday;
	}

	public void setHoliday(boolean holiday) {
		this.holiday = holiday;
	}

	public Date getAdDate() {
		return adDate;
	}
}
