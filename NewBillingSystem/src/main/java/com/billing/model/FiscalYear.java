package com.billing.model;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name = "fiscal_year")
public class FiscalYear {

	@Id
	@NotNull
	@Size(max = 9)
	@Column(name = "fy", unique = true, length = 9)
	private String fy;

	@JsonBackReference
	@OneToMany(mappedBy = "fiscalYear",cascade=CascadeType.ALL)
	List<Invoice> invoices;

	@JsonBackReference
	@OneToMany(mappedBy = "fiscalYear",cascade=CascadeType.ALL)
	List<Refund> refunds;

	@NotNull
	@Column(name = "fy_start_ad", unique = true)
	@Temporal(TemporalType.DATE)
	private Date fyStartAd;
	@NotNull
	@Column(name = "fy_end_ad", unique = true)
	@Temporal(TemporalType.DATE)
	private Date fyEndAd;
	@NotNull
	@Size(max = 10)
	@Column(name = "fy_start_bs", unique = true, length = 10)
	private String fyStartBs;
	@NotNull
	@Size(max = 10)
	@Column(name = "fy_end_bs", unique = true, length = 10)
	private String fyEndBs;
	@NotNull
	@Column(name = "status",length = 5)
	private String status;

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<Invoice> invoices) {
		this.invoices = invoices;
	}

	public String getFy() {
		return fy;
	}

	public void setFy(String fy) {
		this.fy = fy;
	}

	public Date getFyStartAd() {
		return fyStartAd;
	}

	public void setFyStartAd(Date fyStartAd) {
		this.fyStartAd = fyStartAd;
	}

	public Date getFyEndAd() {
		return fyEndAd;
	}

	public void setFyEndAd(Date fyEndAd) {
		this.fyEndAd = fyEndAd;
	}

	public String getFyStartBs() {
		return fyStartBs;
	}

	public void setFyStartBs(String fyStartBs) {
		this.fyStartBs = fyStartBs;
	}

	public String getFyEndBs() {
		return fyEndBs;
	}

	public void setFyEndBs(String fyEndBs) {
		this.fyEndBs = fyEndBs;
	}


	public void setStatus(String status) {
		this.status = status;
	}

	public List<Refund> getRefunds() {
		return refunds;
	}

	public void setRefunds(List<Refund> refunds) {
		this.refunds = refunds;
	}

	public String getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "FiscalYear [fy=" + fy + ", fyStartAd=" + fyStartAd + ", fyEndAd=" + fyEndAd + ", fyStartBs=" + fyStartBs
				+ ", fyEndBs=" + fyEndBs + ", status=" + status + "]";
	}



}
