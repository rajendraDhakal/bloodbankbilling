package com.billing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;


@Entity
public class GroupParameter extends JpaAudit{
    private String groupName;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToMany(cascade = {
            CascadeType.MERGE
    })
    @JoinTable(
            joinColumns = {
                    @JoinColumn(name = "group_parameter_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "item_parameter_id")
            }
    )
    @JsonBackReference
    List<ItemParameter> itemParameters;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ItemParameter> getItemParameters() {
        return itemParameters;
    }

    public void setItemParameters(List<ItemParameter> itemParameters) {
        this.itemParameters = itemParameters;
    }
}
