package com.billing.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import javax.persistence.*;
import java.sql.Date;
import java.util.List;


@Entity
public class Invoice extends JpaAudit{
    private Date date;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer invoiceno;
    private String billto;
    private String name;
    private Integer labno;
    private String paymenttype;
    private String bloodGroup;
    private String unit;
    private String refertype;
    private String item;
    private Number totalamount;
    private Double total;
    private String words;
    private String query;
    private String receivedby;
    private String approvedby;


    @ManyToOne
    @JoinColumn(name="fy_id",nullable = false)
    private FiscalYear fiscalYear;

    @ManyToMany(cascade = {
            CascadeType.MERGE
    }, fetch = FetchType.EAGER)
    @JoinTable(
            joinColumns = {
                    @JoinColumn(name = "invoice_no")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "item_parameter_id")
            }
    )
    @JsonManagedReference
    private List<ItemParameter> itemParameters;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getInvoiceno() {
        return invoiceno;
    }

    public void setInvoiceno(Integer invoiceno) {
        this.invoiceno = invoiceno;
    }

    public String getBillto() {
        return billto;
    }

    public void setBillto(String billto) {
        this.billto = billto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLabno() {
        return labno;
    }

    public void setLabno(Integer labno) {
        this.labno = labno;
    }

    public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRefertype() {
        return refertype;
    }

    public void setRefertype(String refertype) {
        this.refertype = refertype;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Number getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(Number totalamount) {
        this.totalamount = totalamount;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getReceivedby() {
        return receivedby;
    }

    public void setReceivedby(String receivedby) {
        this.receivedby = receivedby;
    }

    public String getApprovedby() {
        return approvedby;
    }

    public void setApprovedby(String approvedby) {
        this.approvedby = approvedby;
    }

    public List<ItemParameter> getItemParameters() {
        return itemParameters;
    }

    public void setItemParameters(List<ItemParameter> itemParameters) {
        this.itemParameters = itemParameters;
    }

    public FiscalYear getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(FiscalYear fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

//    @Override
//    public String toString() {
//        return "Invoice{" +
//                "date=" + date +
//                ", invoiceno=" + invoiceno +
//                ", billto='" + billto + '\'' +
//                ", name='" + name + '\'' +
//                ", labno=" + labno +
//                ", paymenttype='" + paymenttype + '\'' +
//                ", bloodGroup='" + bloodGroup + '\'' +
//                ", unit='" + unit + '\'' +
//                ", refertype='" + refertype + '\'' +
//                ", item='" + item + '\'' +
//                ", totalamount=" + totalamount +
//                ", total=" + total +
//                ", words='" + words + '\'' +
//                ", query='" + query + '\'' +
//                ", receivedby='" + receivedby + '\'' +
//                ", approvedby='" + approvedby + '\'' +
//                ", fiscalYear=" + fiscalYear +
//                ", itemParameters=" + itemParameters +
//                '}';
//    }
}