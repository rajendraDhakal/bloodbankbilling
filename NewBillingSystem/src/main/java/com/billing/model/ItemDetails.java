package com.billing.model;

public class ItemDetails {
	private String description;
	private Integer serialNumber;
	private Number quantity;
	private Number rate;
	private Number amount;
	
	public Integer getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Number getQuantity() {
		return quantity;
	}
	public void setQuantity(Number quantity) {
		this.quantity = quantity;
	}
	public Number getRate() {
		return rate;
	}
	public void setRate(Number rate) {
		this.rate = rate;
	}
	public Number getAmount() {
		return amount;
	}
	public void setAmount(Number amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "ItemDetails [description=" + description + ", serialNumber=" + serialNumber + ", quantity=" + quantity
				+ ", rate=" + rate + ", amount=" + amount + "]";
	}
	

}
