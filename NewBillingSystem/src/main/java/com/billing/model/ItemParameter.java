package com.billing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.List;

import javax.persistence.*;

@Entity
public class ItemParameter extends JpaAudit{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String itemName;
    private String description;
    private Float rate;

    @ManyToMany(mappedBy = "itemParameters", cascade = {CascadeType.ALL})
    @JsonManagedReference
    List<GroupParameter> groupParameters;
    @JsonBackReference
    @ManyToMany(mappedBy = "itemParameters", cascade = {CascadeType.ALL})
    List<Invoice> invoices;
    @JsonBackReference
    @ManyToMany(mappedBy = "itemParameters",cascade = {CascadeType.ALL})
   List<Refund> refund;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }
    public List<GroupParameter> getGroupParameters() {
        return groupParameters;
    }

    public void setGroupParameters(List<GroupParameter> groupParameters) {
        this.groupParameters = groupParameters;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public List<Refund> getRefund() {
        return refund;
    }

    public void setRefund(List<Refund> refund) {
        this.refund = refund;
    }

    @Override
    public String toString() {
        return "ItemParameter{" +
                "id=" + id +
                ", itemName='" + itemName + '\'' +
                ", description='" + description + '\'' +
                ", rate=" + rate +
                ", groupParameters=" + groupParameters +
                ", invoices=" + invoices +
                ", refund=" + refund +
                '}';
    }
}
