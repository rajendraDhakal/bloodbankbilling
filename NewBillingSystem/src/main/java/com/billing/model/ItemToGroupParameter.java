package com.billing.model;

public class ItemToGroupParameter {
	private Integer id;
	private Integer groupCode;
	private String groupName;
	private String item;

	@Override
	public String toString() {
		return "ItemToGroupParameter [groupCode=" + groupCode + ", groupName=" + groupName + ", item=" + item + "]";
	}
	public Integer getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(Integer groupCode) {
		this.groupCode = groupCode;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


}
