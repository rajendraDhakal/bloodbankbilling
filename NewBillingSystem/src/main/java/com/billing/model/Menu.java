package com.billing.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Menu {

	@Id
	@GeneratedValue
	private Integer id;
	@Column
	private String mainMenu;
	@Column
	private String menuName;
	@Column
	private String menuUrl;
	
	@OneToMany(mappedBy = "menu")
	private Set<UserMenu> userMenus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMainMenu() {
		return mainMenu;
	}

	public void setMainMenu(String mainMenu) {
		this.mainMenu = mainMenu;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuUrl() {
		return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	public Set<UserMenu> getUserMenus() {
		return userMenus;
	}

	public void setUserMenus(Set<UserMenu> userMenus) {
		this.userMenus = userMenus;
	}
}
