package com.billing.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;


@Entity
public class Role  implements GrantedAuthority {
	
	private static final long serialVersionUID = 4355146161086268664L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private  Integer id;
	@NotNull
    private  String name;
    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public String getAuthority() {
        return name;
    }


}
