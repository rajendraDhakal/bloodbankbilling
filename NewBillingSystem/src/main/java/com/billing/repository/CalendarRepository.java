package com.billing.repository;


import java.util.Date;
import java.util.List;


import com.billing.model.Calendar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.query.Param;


public interface CalendarRepository extends JpaRepository<Calendar, Date> {
	
	@Query("FROM Calendar C WHERE C.adDate BETWEEN :fyAdStart AND :fyAdEnd")
	List<Calendar> findFiscalYearCalendar(Date fyAdStart, Date fyAdEnd);
	
	@Query("SELECT DISTINCT SUBSTRING(c.bsDate, 1, 4) FROM Calendar c ORDER BY c.bsDate DESC")
	List<String> getCalendarYears();

	@Query("FROM Calendar c WHERE c.bsDate LIKE CONCAT(:year, '%') ORDER BY c.bsDate ASC")
	List<Calendar> findYearlyCalendar(String year);

	@Query("DELETE FROM Calendar c WHERE c.bsDate LIKE CONCAT(:year, '%')")
	boolean deleteCalendar(String year);
	
	@Query("SELECT cl.adDate FROM Calendar cl WHERE cl.bsDate = :bs")
	Date getAdDate(@Param("bs")String bs );

}
