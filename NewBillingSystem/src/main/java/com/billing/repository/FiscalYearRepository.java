package com.billing.repository;


import com.billing.model.FiscalYear;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FiscalYearRepository extends CrudRepository<FiscalYear,String> {
    FiscalYear findByStatus(String status);
}

