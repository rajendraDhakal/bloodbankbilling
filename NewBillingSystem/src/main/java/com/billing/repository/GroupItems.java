package com.billing.repository;

import com.billing.model.GroupParameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupItems extends JpaRepository<GroupParameter, Integer> {

}
