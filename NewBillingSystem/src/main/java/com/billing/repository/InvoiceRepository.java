package com.billing.repository;


import com.billing.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice,Integer> {

    @Query(value = "select invoice from  Invoice invoice where invoice.date between :dateFrom AND :dateTo AND invoice.invoiceno LIKE :billNumber  AND invoice.name LIKE :customerName")
    List<Invoice> findBillBetweenDates(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo,@Param("customerName") String customerName,@Param("billNumber")Integer billNumber);

    @Query(value = "select invoice from  Invoice invoice where invoice.date between :dateFrom AND :dateTo  AND invoice.name LIKE :customerName")
    List<Invoice> findBillBetweenDates(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo,@Param("customerName") String customerName);


    @Query(value = "select invoice from  Invoice invoice where invoice.date between :dateFrom AND :dateTo ")
    List<Invoice> findBillBetweenDates(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);


    @Query(value = "select invoice from  Invoice invoice where invoice.date between :dateFrom AND :dateTo ")
    List<Invoice> findCreditStBetweenDates(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);


    @Query(value = "select invoice from Invoice invoice  where invoice.date = :billDate" )
    List<Invoice> findByInvoiceDate(@Param("billDate") java.sql.Date billDate);

    Optional<Invoice> findByInvoiceno(@Param("id") int id);
    Invoice findByName(@Param("name")String name);

    @Query("select groupParameter.groupName from com.billing.model.GroupParameter groupParameter where groupParameter.id=:id")
    String findGroupNameByID(@Param("id") int id);

}
