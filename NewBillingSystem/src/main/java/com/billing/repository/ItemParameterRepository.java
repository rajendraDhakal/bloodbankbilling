package com.billing.repository;

import java.util.List;

import com.billing.model.ItemParameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface ItemParameterRepository extends JpaRepository<ItemParameter, Integer> {
    //List<ItemParameter> findByGroupParameterId(int id);

    @Query("select itemParameter.rate from ItemParameter itemParameter where itemParameter.id=:id")
    int findRateByID(@Param("id") int id);

    @Query("select itemParameter.itemName from ItemParameter itemParameter where itemParameter.id=:id")
    String findItemNameByID(@Param("id") int id);

    @Query(
            value = "SELECT item_parameter_id FROM billing.group_parameter_item_parameters WHERE group_parameter_id=:groupParameterId",
            nativeQuery = true)
    List<Integer> getItemParameterIdByGroupParametersId(@Param("groupParameterId") int groupParameterId);

    @Query(
            value = "SELECT * FROM billing.item_parameter WHERE id IN :listItemParameterId",
            nativeQuery = true)
    List<ItemParameter> getItemParameterByItemParameterIds(@Param("listItemParameterId") List<Integer> listItemParameterId);


}
