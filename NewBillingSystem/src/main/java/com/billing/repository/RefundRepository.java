package com.billing.repository;

import com.billing.model.Refund;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface RefundRepository extends JpaRepository<Refund, Integer> {


    @Query(value = "select refund from  Refund refund where refund.date between :dateFrom AND :dateTo AND refund.invoiceno LIKE :refundNo AND refund.name LIKE :customerName")
    List<Refund> findRefundBetweenDates(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo,@Param("customerName") String customerName,@Param("refundNo") Integer refundNo );


    @Query(value = "select refund from  Refund refund where refund.date between :dateFrom AND :dateTo  AND refund.name LIKE :customerName")
    List<Refund> findRefundBetweenDates(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo,@Param("customerName") String customerName);

    @Query(value = "select refund from  Refund refund where refund.date between :dateFrom AND :dateTo")
    List<Refund> findRefundBetweenDates(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);
//    Invoice findByInvoiceno(@Param("id") int id);
}
