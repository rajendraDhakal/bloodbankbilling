package com.billing.seeder;

import com.billing.service.impl.RoleServiceImpl;
import com.billing.model.Role;
import com.billing.model.User;
import com.billing.repository.UserRepository;
import com.billing.security.PasswordEncoderGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UserTableSeeder implements CommandLineRunner {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleServiceImpl roleService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public void run(String... args) throws Exception {
        PasswordEncoderGenerator passwordEncoderGenerator = new PasswordEncoderGenerator();
        User user = new User();
        Set<Role> roleSetAdmin = new HashSet<>();
        int count = 1;
       // System.out.print("passhash"+passwordEncoderGenerator.passwordHash("ADMIN"));
        user.setUsername("ADMIN");
        user.setPassword(bCryptPasswordEncoder.encode("admin"));
        user.setFullName("Rajendra Dhakal");
        user.setRoles(this.getAllRoles());
        //System.out.println("password--->"+user.getPassword());
        while(count == 1){
           // User userRecord = userRepository.save(user);
            count++;
        }
    }
    public Set<Role> getAllRoles(){
       Set<Role> roleSet = new HashSet<>();
       List<Role> roles = roleService.getAllRoles();
       for (Role role : roles) {
           if(role.getName().equalsIgnoreCase("ADMIN"))
                roleSet.add(role);

       }
       return roleSet;
    }
}
