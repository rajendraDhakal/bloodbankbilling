package com.billing.service;

import com.billing.model.Invoice;

public interface BillApiService {
    public Invoice reportFilter(Invoice reports);
}
