package com.billing.service;

import java.util.Date;
import java.util.List;

import com.billing.model.Calendar;



public interface CalendarService {
	
	List<Calendar> getFiscalYearCalendar(Date fyAdStart, Date fyAdEnd);
	
	boolean generateAllCalendar(List<Calendar> cList);
	
	boolean alreadyExists(Date adDate);
	
	List<String> getAllYears();
	
	List<Calendar> getYearCalendar(String year);
	
	boolean deleteCalendar(Calendar c);

}
