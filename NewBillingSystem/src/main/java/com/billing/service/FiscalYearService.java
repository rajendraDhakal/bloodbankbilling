package com.billing.service;

import java.util.Date;
import java.util.List;

import com.billing.model.FiscalYear;



public interface FiscalYearService {

	List<FiscalYear> getFiscalYear();

	public FiscalYear getFiscalYearByFy(String fy);

	public boolean addFY(FiscalYear fy);
//	public FiscalYear saveStatus(FiscalYear fiscalYear);

	public boolean deleteFY(FiscalYear fy);

	public void updateFY(FiscalYear fy);

	public Date getAD(String bsDate);
	public FiscalYear getCurrentFiscalYear(String status);

}
