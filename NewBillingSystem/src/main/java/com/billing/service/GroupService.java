package com.billing.service;

import com.billing.model.GroupParameter;

public interface GroupService {
    public GroupParameter groupSave(GroupParameter groupParameter);

}
