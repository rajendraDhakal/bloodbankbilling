package com.billing.service;

import com.billing.dto.BillListDto;
import com.billing.model.Invoice;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface InvoiceService {
    public  Invoice RemoveRowInvoice (Invoice invoice);
    public Invoice saveInvoice(Invoice invoiceSave);
    public Optional<Invoice> searchByInvoiceId(Integer id);
    public List<BillListDto> findInvoiceByNameOrInvoiceNo(Date dateFrom,Date dateTo,String name,String invoiceno);

}
