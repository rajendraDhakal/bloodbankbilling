package com.billing.service;
import com.billing.model.ItemParameter;



public interface ItemParameterService  {
    public ItemParameter saveItemParameter(ItemParameter itemParameter);
}
