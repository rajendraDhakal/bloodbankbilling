package com.billing.service;

import com.billing.model.GroupParameter;

public interface ItemToGroupService {
    public GroupParameter saveItemToGroup(GroupParameter groupItem);
}
