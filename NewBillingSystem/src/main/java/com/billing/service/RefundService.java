package com.billing.service;

import com.billing.model.Refund;

public interface RefundService {
    public Refund  saveRefund(Refund refundSave);
    public Refund  removeUncheckedItemParameters(Refund refund);
}
