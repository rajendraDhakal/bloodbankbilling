package com.billing.service;

import java.util.List;

import com.billing.model.Role;



public interface RoleService {
	
	List<Role> getAllRoles();

}
