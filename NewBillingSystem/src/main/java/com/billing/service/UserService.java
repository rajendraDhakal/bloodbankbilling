package com.billing.service;




import com.billing.dto.ChangePasswordDTO;
import com.billing.model.User;

public interface UserService {

	User isValid(String username, String oldPassword);
	
	boolean changePassword(ChangePasswordDTO pDto);

}
