package com.billing.service.impl;

import java.util.Date;

import java.util.List;

import javax.transaction.Transactional;


import com.billing.model.Calendar;
import com.billing.service.CalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.billing.repository.CalendarRepository;

@Service
@Transactional
public class CalendarServiceImpl implements CalendarService {

	@Autowired
	private CalendarRepository cRepo;

	@Override
	public List<Calendar> getFiscalYearCalendar(Date fyAdStart, Date fyAdEnd) {
		return cRepo.findFiscalYearCalendar(fyAdStart, fyAdEnd);
	}

	@Override
	public boolean generateAllCalendar(List<Calendar> cList) {
		try {
			for(Calendar c : cList) {
				if(alreadyExists(c.getAdDate())){
					System.out.println(c);
					return false;
				}
			}

			cRepo.saveAll(cList);
			return true;			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean alreadyExists(Date adDate) {
		if(cRepo.existsById(adDate)) {
			System.out.println(adDate);
			return true;
		}
		return false;
	}

	@Override
	public List<String> getAllYears() {
		return cRepo.getCalendarYears();
	}

	@Override
	public List<Calendar> getYearCalendar(String year) {
		return cRepo.findYearlyCalendar(year);
	}

	@Override
	public boolean deleteCalendar(Calendar c) {
		try {
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}

}
