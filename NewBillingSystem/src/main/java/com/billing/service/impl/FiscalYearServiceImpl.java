package com.billing.service.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;


import com.billing.model.FiscalYear;
import com.billing.repository.CalendarRepository;
import com.billing.repository.FiscalYearRepository;
import com.billing.service.FiscalYearService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class FiscalYearServiceImpl implements FiscalYearService {
//	private static final org.jboss.logging.Logger log = LoggerFactory.logger(FiscalYearServiceImpl.class);

	@Autowired
	private FiscalYearRepository fYearRepo;
	@Autowired
	private CalendarRepository calRepo;
	
	@Override
	public List<FiscalYear> getFiscalYear() {
		System.out.println("FiscalYear Override");
		try {
			System.out.println("here");
			return (List<FiscalYear>) fYearRepo.findAll();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		
			return null;
			// TODO: handle exception
		}
		
	}
	
	@Override
	public FiscalYear getFiscalYearByFy(String fy) {
		FiscalYear fiscalYear = fYearRepo.findById(fy).get();
		return fiscalYear;
	}
	
	@Override
	@Transactional
	public boolean addFY(FiscalYear fy) {
		try {
			fYearRepo.save(fy);
			return true;
		}catch(Exception ex) {
			System.out.println("save"+ex.getMessage());
			return false;
		}
	}
//
//	@Override
//	public FiscalYear saveStatus(FiscalYear fiscalYear) {
//		fYearRepo.save(fiscalYear);
//		return null;
//	}

	@Override
	public boolean deleteFY(FiscalYear fy) {
		try {
			fYearRepo.delete(fy);
			return true;
		}catch(Exception ex) {
			return false;
		}
		
	}

	@Override
	public void updateFY(FiscalYear fy) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public Date getAD(String bsDate) {
		try {
			return calRepo.getAdDate(bsDate);
			
		} catch (Exception e) {
			System.out.println( "exception"+ e.getMessage());
			return null;
		}
		
		
	}

	@Override
	public FiscalYear getCurrentFiscalYear(String status) {
		FiscalYear currentFiscalYear = fYearRepo.findByStatus(status);
		return currentFiscalYear;
	}


}
