package com.billing.service.impl;
import com.billing.model.GroupParameter;
import com.billing.service.GroupService;
import com.billing.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GroupServiceImpl implements GroupService {
    @Autowired
    GroupRepository groupRepository;

    @Override
    public GroupParameter groupSave(GroupParameter groupParameter) {
        groupRepository.save(groupParameter);
        return null;
    }
}
