package com.billing.service.impl;


import com.billing.dto.BillListDto;
import com.billing.model.Invoice;
import com.billing.model.ItemParameter;
import com.billing.repository.GroupRepository;
import com.billing.repository.InvoiceRepository;
import com.billing.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    GroupRepository groupRepository;

    @Override
    public Invoice RemoveRowInvoice(Invoice invoice) {
        Invoice invoicetoprint = new Invoice();
        List<ItemParameter> itemParametertoPrint = new ArrayList<>();
        invoicetoprint.setApprovedby(invoice.getApprovedby());
        invoicetoprint.setDate(invoice.getDate());
        invoicetoprint.setInvoiceno(invoice.getInvoiceno());
        invoicetoprint.setName(invoice.getName());
        invoicetoprint.setReceivedby(invoice.getReceivedby());
        invoicetoprint.setTotal(invoice.getTotal());
        invoicetoprint.setWords(invoice.getWords());
        invoicetoprint.setUnit(invoice.getUnit());
        invoicetoprint.setBillto(invoice.getBillto());
        invoicetoprint.setItem(invoice.getItem());
        invoicetoprint.setPaymenttype(invoice.getPaymenttype());
        invoicetoprint.setRefertype(invoice.getRefertype());
        for (ItemParameter items : invoice.getItemParameters()) {
            if (items.getId() != null)
                itemParametertoPrint.add(items);

            else {
                items.setId(10);

            }

        }
        invoicetoprint.setItemParameters(itemParametertoPrint);

        return invoicetoprint;
    }

    @Override
    public Invoice saveInvoice(Invoice invoiceSave) {
        invoiceRepository.save(invoiceSave);
        return null;
    }

    @Override
    public Optional<Invoice> searchByInvoiceId(Integer id) {
        Optional<Invoice> invoice = invoiceRepository.findByInvoiceno(id);
        return invoice;
    }


//
//    @Override
//    public Invoice searchByInvoiceId(Integer id) {
//        Invoice invoice = invoiceRepository.findByInvoiceno(id);
//        return invoice;
//    }

    @Override
    public List<BillListDto> findInvoiceByNameOrInvoiceNo(Date dateFrom, Date dateTo, String name, String invoiceno) {
        return null;
    }


}