package com.billing.service.impl;


import com.billing.model.ItemParameter;
import com.billing.repository.ItemParameterRepository;
import com.billing.service.ItemParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemParameterServiceImpl implements ItemParameterService {
    @Autowired
    ItemParameterRepository itemParameterRepository;

    @Override
    public ItemParameter saveItemParameter(ItemParameter itemParameter) {
        itemParameterRepository.save(itemParameter);
        return null;
    }
}
