package com.billing.service.impl;
import com.billing.model.GroupParameter;
import com.billing.repository.GroupRepository;
import com.billing.service.ItemToGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ItemToGroupServiceImpl implements ItemToGroupService {
    @Autowired
    GroupRepository groupRepository;

    @Override
    public GroupParameter saveItemToGroup(GroupParameter groupItem) {
        groupRepository.save(groupItem);
        return null;
    }
}
