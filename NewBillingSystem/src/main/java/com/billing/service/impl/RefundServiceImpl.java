package com.billing.service.impl;


import com.billing.model.ItemParameter;
import com.billing.model.Refund;
import com.billing.service.RefundService;
import com.billing.repository.RefundRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RefundServiceImpl implements RefundService {

    @Autowired
    RefundRepository refundRepository;

    @Override
    public Refund saveRefund(Refund refundSave) {
        Refund refund = refundRepository.save(refundSave);
        return refund;
    }

    @Override
    public Refund removeUncheckedItemParameters(Refund refund) {

        Refund refundToPrint = new Refund();
        List<ItemParameter> itemParametertoPrint = new ArrayList<>();
        refundToPrint.setApprovedby(refund.getApprovedby());
        refundToPrint.setDate(refund.getDate());
        refundToPrint.setInvoiceno(refund.getInvoiceno());
        refundToPrint.setName(refund.getName());
        refundToPrint.setReceivedby(refund.getReceivedby());
        refundToPrint.setTotal(refund.getTotal());
        refundToPrint.setWords(refund.getWords());
        refundToPrint.setUnit(refund.getUnit());
        refundToPrint.setAmount(refund.getAmount());
        refundToPrint.setBillto(refund.getBillto());
        refundToPrint.setItem(refund.getItem());
        refundToPrint.setPaymenttype(refund.getPaymenttype());
        refundToPrint.setRefertype(refund.getRefertype());
        for (ItemParameter items : refund.getItemParameters()) {
            if (items.getId() != null)
                itemParametertoPrint.add(items);
        }
        refundToPrint.setItemParameters(itemParametertoPrint);

        return refundToPrint;
    }


}
