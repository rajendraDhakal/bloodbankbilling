package com.billing.service.impl;


import com.billing.service.RetriveCurrentDateService;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class RetriveCurrentDateServiceImpl implements RetriveCurrentDateService {

    @Override
    public Date currentDate() {
        Date date = new Date(new java.util.Date().getTime());
        System.out.println("date="+date);
        return date;
    }
}
