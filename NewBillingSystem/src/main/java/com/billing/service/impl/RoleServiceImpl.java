package com.billing.service.impl;

import java.util.List;

import com.billing.service.RoleService;
import com.billing.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.billing.repository.RoleRepository;

@Service
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleRepository roleRepo;

	@Override
	public List<Role> getAllRoles() {
		return roleRepo.findAll();
	}

}
