package com.billing.service.impl;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import com.billing.model.User;
import com.billing.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UserDetailServiceImpl implements UserDetailsService {
	
	@Autowired
	HttpSession session;
    
	@Autowired
    UserRepository userRepository;
    
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("Inside USerdetailsService");
        User user = userRepository.findByUsername(username);
        
        if(user==null){
            throw new UsernameNotFoundException("User not found for Username"+username);
        }
        session.setAttribute("sessionUser", user);
        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),user.getRoles());

    }

}
