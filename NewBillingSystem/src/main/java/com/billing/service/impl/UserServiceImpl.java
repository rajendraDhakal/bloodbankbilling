package com.billing.service.impl;

import javax.transaction.Transactional;

import com.billing.dto.ChangePasswordDTO;
import com.billing.model.User;
import com.billing.repository.UserRepository;
import com.billing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
    UserRepository userRepository;

	@Override
	public User isValid(String username, String oldPassword) {
		
		try {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

			User u = userRepository.findByUsername(username);

			String dbPassword = u.getPassword();
			
			if (passwordEncoder.matches(oldPassword, dbPassword)) {				
				return u;
			}
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean changePassword(ChangePasswordDTO user) {
		try {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

			String newPassword = user.getNewPassword();

			User u = isValid(user.getUsername(), user.getOldPassword());

			if (u != null) {
				u.setPassword(passwordEncoder.encode(newPassword));
				userRepository.save(u);
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

}
