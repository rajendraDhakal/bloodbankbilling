package com.billing.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.billing.dto.CalendarDTO;


public class CalendarGenerator {


	public static List<com.billing.model.Calendar> calendarGenerator(CalendarDTO calendar) throws Exception {

		String DAY_OF_WEEK, bsDate = "", adDate = "";
		int month[] = new int[13], totalDay = 0;;
		String MM[] = { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
		String DD[] = { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15",
				"16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32" };
		Calendar cal = Calendar.getInstance();
		Date date = new Date();
		SimpleDateFormat dayOfWeek = new java.text.SimpleDateFormat("E");
		
		List<com.billing.model.Calendar> cList = new ArrayList<>();

		month[1] = calendar.getBaisakh();
		month[2] = calendar.getJestha();
		month[3] = calendar.getAshar();
		month[4] = calendar.getShrawan();
		month[5] = calendar.getBhadra();
		month[6] = calendar.getAsoj();
		month[7] = calendar.getKartik();
		month[8] = calendar.getMangsir();
		month[9] = calendar.getPoush();
		month[10] = calendar.getMagh();
		month[11] = calendar.getFalgun();
		month[12] = calendar.getChaitra();

		bsDate = calendar.getBsStart();
		adDate = calendar.getAdStart();
		String bsYear = bsDate.substring(0, 4);

		LocalDate lDate = LocalDate.parse(adDate);

		int year = lDate.getYear();
		int mon = lDate.getMonthValue()-1;
		int day = lDate.getDayOfMonth();

		try {
			for (int i = 1; i < MM.length; i++) {	
				for (int j = 1; j <= month[i]; j++) {
					com.billing.model.Calendar c = new com.billing.model.Calendar();
					
					bsDate = bsYear + "-" + MM[i] + "-" + DD[j];
					
					cal.set(year, mon, day+totalDay);
					date = cal.getTime();					 
					
					DAY_OF_WEEK = dayOfWeek.format(date);
					
					if (DAY_OF_WEEK.equalsIgnoreCase("Sat")) {
						c.setHoliday(true);
                    } else {
                    	c.setHoliday(false);
                    }
					
					c.setBsDate(bsDate);
					c.setAdDate(date);
					c.setDay(DAY_OF_WEEK);
					
					cList.add(c);
					
					totalDay++;
//					System.out.println(c.getBsDate()+" :: "+c.getAdDate()+" :: "+c.getDay()+" :: "+c.isHoliday());
				}
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
		return cList;
	}

}
