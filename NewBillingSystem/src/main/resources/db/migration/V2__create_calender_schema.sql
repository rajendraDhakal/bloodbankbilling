CREATE TABLE IF NOT EXISTS `calendar` (
  `ad_date` date NOT NULL,
  `bs_date` varchar(10) NOT NULL,
  `day` varchar(10) NOT NULL,
  `holiday` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ad_date`),
  UNIQUE KEY `UK_2y4unrrre0q3om07af14wfkwk` (`bs_date`)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;