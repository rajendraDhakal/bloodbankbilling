CREATE TABLE IF NOT EXISTS `fiscal_year` (
  `fy` varchar(9) NOT NULL,
  `fy_end_ad` date NOT NULL,
  `fy_end_bs` varchar(10) NOT NULL,
  `fy_start_ad` date NOT NULL,
  `fy_start_bs` varchar(10) NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`fy`),
  UNIQUE KEY `UK_qbqj03i7dkbsvdl2ht7wjc562` (`fy_end_ad`),
  UNIQUE KEY `UK_nj6anv79g7spq2w6qkhuahsgn` (`fy_end_bs`),
  UNIQUE KEY `UK_9r6rb88gpunld7meaf99inrrd` (`fy_start_ad`),
  UNIQUE KEY `UK_ccbdnyypuaefllb8xlcw0q90y` (`fy_start_bs`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;