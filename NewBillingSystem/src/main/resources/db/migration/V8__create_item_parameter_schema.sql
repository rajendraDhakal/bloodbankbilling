CREATE TABLE IF NOT EXISTS `item_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `refund_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8rkie13o0pyvpw1sy6skvt7nj` (`refund_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=UTF8;