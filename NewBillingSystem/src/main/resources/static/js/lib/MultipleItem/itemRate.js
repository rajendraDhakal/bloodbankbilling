function format(d) {
    // `d` is the original data object for the row
    console.log(d);
    var tableHead ='<tr>' +
        '<td>ItemName</td>' +
        '<td>Unit</td>' +
        '<td>Rate</td>' +
        '</tr>' ;
    var tr = '';
    var unit = d.unit;

    d.refundItemDtoList.forEach(function (item) {
        tr += '<tr>' +
            '<td>' + item.itemName + '</td>' +
            '<td>' +unit+ '</td>' +
            '<td>' + item.rate + '</td>' +
            '</tr>';

    });

    return '<table  class="table table-bordered" cellpadding="5" cellspacing="0" border="1" align="center">' +tableHead+
        tr +
        '</table>';
}


function dt(data1) {
    var table = $('#refundDateBetween').DataTable({
        data: data1,
        order: [ [1, 'asc'] ],
        columns: [
            {
                className: 'details-control',
                orderable: false,
                data: null,
                defaultContent: ''
            },
            {
                "data": "invoiceno"
            }, {
                "data": "name"
            }, {
                "data": "paymenttype"
            }, {
                "data": "refertype"
            }, {
                "data": "total"
            }
        ],
    });

    // Add event listener for opening and closing details
    $('#refundDateBetween tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            d = row.data();
            row.child(format(d)).show();
            tr.addClass('shown');
        }
    });
}

function dateSearch() {
    var dateFrom = document.getElementById("dateFrom").value;
    var dateTo = document.getElementById("dateTo").value;
    var name = document.getElementById("customerName").value;
   // console.log("name"+customerName);
    var invoiceno = document.getElementById("refundNo").value;
    //  getjson data from api using ajax into data1
    $.ajax({

        url: "../api/getRefundBetweenDates?dateFrom=" + dateFrom + "&dateTo=" + dateTo + "&customerName="+ name + "&refundNo="+ invoiceno,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (json) {
            dt(json);
        },
        error: function (e) {
            console.log(e);
        }
    });

}

