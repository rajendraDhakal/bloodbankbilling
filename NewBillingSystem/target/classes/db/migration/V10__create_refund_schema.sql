CREATE TABLE IF NOT EXISTS `refund` (
  `id` int(11) NOT NULL,
  `approvedby` varchar(255) DEFAULT NULL,
  `billto` varchar(255) DEFAULT NULL,
  `blood_group` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `invoiceno` int(11) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `labno` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `paymenttype` varchar(255) DEFAULT NULL,
  `query` varchar(255) DEFAULT NULL,
  `receivedby` varchar(255) DEFAULT NULL,
  `refertype` varchar(255) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `words` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;
