CREATE TABLE IF NOT EXISTS `user_menu` (
  `id` bigint(20) NOT NULL,
  `status` bit(1) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6ai9646ro1cy8b9wrproflr27` (`menu_id`),
  KEY `FKsertl83ph2qbdqtr7lgraxavb` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;